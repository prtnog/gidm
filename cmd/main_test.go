package main

import (
	"bytes"
	"testing"
)

func TestHelp(t *testing.T) {
	buf := bytes.NewBuffer(nil)
	help(buf)

	const want = "gidp is not implemented yet\n"
	got := buf.String()
	if got != want {
		t.Errorf("help() = %s; want %s", got, want)
	}
}
